// eslint-disable-line import/no-extraneous-dependencies, import/no-unresolved
import R from 'ramda'
import config from './config'
import initDebug from './debug'
import { isNumberRegistered, deleteNumber } from './client'
import {
  configLoader, isBlank, makeError, makeResult,
} from './http'

initDebug()

// eslint-disable-next-line no-unused-vars,prefer-arrow-callback
export default configLoader(async function _delete(event, context) {
  if (isBlank(config.SIGNAL_SHARED_SECRET)) {
    return makeError(401, 'administrator must provide a \'SIGNAL_SHARED_SECRET\' env variable')
  }

  const { secret } = event.queryStringParameters
  if (secret !== config.SIGNAL_SHARED_SECRET) {
    return makeError(401, 'not authorized')
  }

  let body
  try {
    body = JSON.parse(event.body)
  } catch (err) {
    console.error(err)
    return makeError(400, 'failed to parse request body as json')
  }

  if (R.isNil(body.number)) {
    return makeError(400, 'invalid request')
  }

  const { number } = body

  const alreadyRegistered = await isNumberRegistered(number)
  if (!alreadyRegistered) {
    return makeError(400, `the number '${number}' is not registered`)
  }

  try {
    await deleteNumber(number)
    return makeResult(200, `the number '${number}' is deleted`)
  } catch (err) {
    console.log(`failed to delete number ${number}`)
    console.error(err)
    return makeError(500, 'failed to delete number')
  }
})
