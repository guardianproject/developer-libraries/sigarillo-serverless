/* eslint-disable import/prefer-default-export */
export function logify(data) {
  let line = ''


  Object.keys(data).forEach((key) => {
    let value = data[key]
    let isNull = false
    if (value == null) {
      isNull = true
      value = ''
    } else value = value.toString()

    const needsQuoting = value.indexOf(' ') > -1 || value.indexOf('=') > -1
    const needsEscaping = value.indexOf('"') > -1 || value.indexOf('\\') > -1

    if (needsEscaping) value = value.replace(/["\\]/g, '\\$&')
    if (needsQuoting) value = `"${value}"`
    if (value === '' && !isNull) value = '""'

    line += `${key}=${value} `
  })

  // trim trailing space
  return line.substring(0, line.length - 1)
}
