import R from 'ramda'
import config from './config'
import initDebug from './debug'
import { isNumberRegisteredAndVerified, verifyNumber } from './client'
import {
  configLoader, isBlank, makeError, makeResult,
} from './http'

initDebug()

// eslint-disable-next-line no-unused-vars,prefer-arrow-callback
export default configLoader(async function verify(event, context) {
  if (isBlank(config.SIGNAL_SHARED_SECRET)) {
    return makeError(401, 'administrator must provide a \'SIGNAL_SHARED_SECRET\' env variable')
  }

  const { secret } = event.queryStringParameters
  if (secret !== config.SIGNAL_SHARED_SECRET) {
    return makeError(401, 'not authorized')
  }

  let body
  try {
    body = JSON.parse(event.body)
  } catch (err) {
    console.error(err)
    return makeError(400, 'failed to parse request body as json')
  }

  if (R.isNil(body.number) || R.isNil(body.code)) {
    return makeError(400, 'invalid request')
  }

  const { number, code } = body

  const alreadyRegistered = await isNumberRegisteredAndVerified(number)
  if (alreadyRegistered) {
    return makeError(400, `the number '${number}' is already registered and verified`)
  }

  try {
    const numberHash = await verifyNumber(number, code)
    return makeResult(200, `the number '${number}' is now verified and ready to use`, { numberId: numberHash })
  } catch (err) {
    console.log(`failed to verify number ${number}`)
    console.error(err)
    return makeError(500, 'failed to verify number')
  }
})
