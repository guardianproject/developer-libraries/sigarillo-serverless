// eslint-disable-line import/no-extraneous-dependencies, import/no-unresolved
import R from 'ramda'
import config from './config'
import initDebug from './debug'
import { sendAlert } from './client'
import { parseAlerts, getRecipientForReceiver } from './alertmanager'
import {
  configLoader, isBlank, makeError, makeResult,
} from './http'

initDebug()

// eslint-disable-next-line no-unused-vars,prefer-arrow-callback
export default configLoader(async function alert(event, context) {
  if (isBlank(config.SIGNAL_SHARED_SECRET)) {
    return makeError(401, 'administrator must provide a \'SIGNAL_SHARED_SECRET\' env variable')
  }

  const { secret } = event.queryStringParameters
  if (secret !== config.SIGNAL_SHARED_SECRET) {
    return makeError(401, 'not authorized')
  }

  const { sender } = event.queryStringParameters
  if (isBlank(sender)) {
    return makeError(400, 'no sender number specified in query params')
  }

  let body
  try {
    body = JSON.parse(event.body)
  } catch (err) {
    console.error(err)
    return makeError(400, 'failed to parse request body as json')
  }

  const recipient = getRecipientForReceiver(body.receiver)
  if (R.isNil(recipient)) {
    return makeError(404, `no signal recipient found for receiver ${body.receiver}`)
  }

  const alerts = parseAlerts(body)
  if (R.isEmpty(alerts)) {
    return makeResult(200, 'no alerts in payload')
  }

  try {
    await Promise.all(alerts.map(async alertData => sendAlert(sender, recipient, alertData)))
    return makeResult(200, 'ok')
  } catch (err) {
    console.error(err)
    return makeError(500, `failed sending alert from ${sender} to ${recipient}. see logs for more information.`)
  }
})
