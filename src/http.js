import R from 'ramda'
import config from './config'

export const isBlank = R.anyPass([R.isNil, R.isEmpty, R.test(/^(?!.*\S)/)])
export const makeResult = (code, result, extra = {}) => ({
  statusCode: code,
  body: JSON.stringify(R.mergeRight({ result }, extra)),
})

export const makeError = (code, error) => ({
  statusCode: code,
  body: JSON.stringify({ error }),
})

export const configLoader = f => async (event, context) => {
  await config.readyPromise
  return f(event, context)
}
