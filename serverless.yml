---
# For full config options, check the docs:
#    docs.serverless.com

service: sigarillo-serverless
custom:
  # Our stage is based on what is passed in when running serverless
  # commands. Or falls back to what we have set in the provider section.
  stage: ${opt:stage, self:provider.stage}
  tableName: ${self:service}-${opt:stage, self:provider.stage}
    # Set our DynamoDB throughput for prod and all other non-prod stages.
  tableThroughputs:
    production: 2
    default: 1
  tableThroughput: ${self:custom.tableThroughputs.${self:custom.stage}, self:custom.tableThroughputs.default}
  environment: ${file(env.yml):${self:custom.stage}, file(env.yml):default}
  ssmPrefix: ${self:service}/${self:custom.stage}

provider:
  name: aws
  runtime: nodejs8.10
  stage: development
  region: eu-central-1

  environment:
    SIGNAL_DYNAMODB_TABLE:  ${self:custom.tableName}
    SSM_PREFIX: ${self:custom.ssmPrefix}
    NODE_ENV: production
  iamRoleStatements:
    - Effect: Allow
      Action:
        - kms:Decrypt
      Resource:
        - ${self:custom.environment.kmsKeyArn}
    - Effect: Allow
      Action:
        - ssm:GetParameters
        - ssm:GetParametersByPath
      Resource:
        - 'arn:aws:ssm:${self:provider.region}:#{AWS::AccountId}:parameter/${self:custom.ssmPrefix}/*'
    - Effect: Allow
      Action:
        - dynamodb:Query
        - dynamodb:Scan
        - dynamodb:GetItem
        - dynamodb:PutItem
        - dynamodb:UpdateItem
        - dynamodb:DeleteItem
      Resource:
        - "Fn::GetAtt": [ SignalDynamoDbTable, Arn ]

functions:
  register:
    handler: src/index.register
    events:
      - http:
          path: register
          method: POST
          cors: true
  verify:
    handler: src/index.verify
    events:
      - http:
          path: verify
          method: POST
          cors: true
  delete:
    handler: src/index.delete
    events:
      - http:
          path: delete
          method: POST
          cors: true
  alert:
    handler: src/index.alert
    events:
      - http:
          path: alert
          method: POST
          cors: true

resources:
  Resources:
    SignalDynamoDbTable:
      Type: 'AWS::DynamoDB::Table'
      #DeletionPolicy: Retain
      Properties:
        TableName: ${self:custom.tableName}
        AttributeDefinitions:
          - AttributeName: id
            AttributeType: S
        KeySchema:
          - AttributeName: id
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: ${self:custom.tableThroughput}
          WriteCapacityUnits: ${self:custom.tableThroughput}

plugins:
  - serverless-pseudo-parameters
