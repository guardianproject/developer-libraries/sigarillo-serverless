#!/usr/bin/env bash
# This script is designed to be run by Vagrant on a fresh centox 7 box.

set -ex

# Install system packages #####################################################

echo 'Checking for Development Tools... '
sudo yum -y update
sudo yum -yq makecache
sudo yum -yq groups mark install "Development Tools"
sudo yum -yq groups mark convert "Development Tools"
sudo yum -yq groupinstall "Development Tools"
sudo yum -yq install wget openssl-devel python-devel libffi-devel curl htop ncdu vim
echo 'OK'


# Install Python 3 with Miniconda #############################################

echo 'Checking for Miniconda Python... '
if ! conda --version; then
  wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
  bash Miniconda3-latest-Linux-x86_64.sh -b
  rm -rf Miniconda3-latest-Linux-x86_64.sh
  export PATH=/home/vagrant/miniconda3/bin:$PATH

  echo '# Miniconda Python' >> .bashrc
  echo "export PATH=/home/vagrant/miniconda3/bin:$PATH" >> .bashrc
else
  echo 'OK'
fi


# Install AWS CLI #############################################################

echo "Checking for AWS CLI... "
if ! aws --version; then
  pip install awscli
else
  echo 'OK'
fi


# Install Node Version Manager ################################################

echo "Checking for NVM... "
if [[ ! -x /home/vagrant/.nvm ]]; then
  cd /home/vagrant
  git clone https://github.com/creationix/nvm.git .nvm
  cd ~/.nvm
  git checkout v0.34.0

  export NVM_DIR="/home/vagrant/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
  [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

  cat << EOF >> ~/.bashrc
export NVM_DIR="/home/vagrant/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
EOF
  echo '' >> ~/.bashrc
  nvm --version
  echo "OK"
else 
 export NVM_DIR="/home/vagrant/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
  [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
  nvm --version
  echo "OK"
fi


# Install Node.js #############################################################

echo "Checking for Node.js... "
if ! node --version; then
  nvm install 8.10
  nvm use 8.10
else
  echo 'OK'
fi


# Install Serverless & plugins ################################################

echo "Checking for Serverless Framework... "
if ! serverless --version; then
  # Install Serverless as global package
  npm install serverless -g
  # Link to emulate serverless AWS TASK_ROOT path


  [ -L /var/task ] ||  /sudo ln -s /vagrant /var/task
  echo '' >> ~/.bashrc
else
  echo 'OK'
fi


# Install direnv ##############################################################

echo "Checking for direnv... "
if ! direnv; then
  sudo wget https://github.com/direnv/direnv/releases/download/v2.19.2/direnv.linux-amd64 -O /usr/local/bin/direnv
  sudo chmod +x /usr/local/bin/direnv

  echo '' >> ~/.bashrc
  echo '# Hook direnv' >> ~/.bashrc
  echo 'eval "$(direnv hook bash)"' >> ~/.bashrc
else
  echo 'OK'
fi


# Install aws-vault ###########################################################
if ! aws-vault; then
  sudo wget https://github.com/99designs/aws-vault/releases/download/v4.5.1/aws-vault-linux-amd64 -O /usr/local/bin/aws-vault
  sudo chmod +x  /usr/local/bin/aws-vault
fi

# Colorize Prompt #############################################################
set +e
rc=$(grep PS1 ~/.bashrc)
set -e
if [ "$rc" != "0" ]; then
  cat << EOF >> ~/.bashrc
export PS1='\[\033[02;32m\]\u@\H:\[\033[02;34m\]\w\$\[\033[00m\] '
EOF
fi

# Clean Yum cache #############################################################

yum clean all
