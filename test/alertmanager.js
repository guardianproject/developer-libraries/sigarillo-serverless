/* eslint-disable no-unused-expressions */
import * as chai from 'chai'
import {
  describe, it,
} from 'mocha'

import { parseAlerts, getRecipientForReceiver } from '../src/alertmanager'
import { alertsPayload } from './fixtures'

const { expect } = chai

describe('alertmanager parseAlerts', () => {
  it('empty payload to give empty list', async () => {
    const payload = {}
    const result = parseAlerts(payload)
    expect(result).to.be.an('array').that.is.empty
  })

  it('empty alerts list in payload to give empty list', async () => {
    const payload = { alerts: [] }
    const result = parseAlerts(payload)
    expect(result).to.be.an('array').that.is.empty
  })

  it('valid payload is formatted', async () => {
    const result = parseAlerts(alertsPayload)
    expect(result).to.have.lengthOf(2)
  })
})
describe('alertmanager getRecipientForReceiver', () => {
  it('returns nil for a non existent receiver', async () => {
    const result = getRecipientForReceiver('receiver3', 'receiver1=+111,receiver2=+222')
    expect(result).to.be.undefined
  })
  it('parses a recipients mapping string', async () => {
    const result = getRecipientForReceiver('receiver1', 'receiver1=+111,receiver2=+222')
    expect(result).to.eq('+111')
  })
  it('parses a recipients mapping string with just one entry', async () => {
    const result = getRecipientForReceiver('receiver1', 'receiver1=+111')
    expect(result).to.eq('+111')
  })
})
