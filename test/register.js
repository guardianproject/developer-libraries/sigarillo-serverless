import * as chai from 'chai'
import sinon from 'sinon'
import {
  describe, it, beforeEach, afterEach,
} from 'mocha'
import register from '../src/register'
import * as client from '../src/client'
import * as config from '../src/config'

const { expect } = chai

describe('register testing inputs', () => {
  let sandbox
  let clientMock
  beforeEach((done) => {
    sandbox = sinon.createSandbox()
    sandbox.stub(config, 'default').value({
      SIGNAL_SHARED_SECRET: 'hello',
      SIGNAL_ALERTMANAGER_RECIPIENTS: 'receiver1=+111,receiver2=+222',
    })
    clientMock = sinon.mock(client)
    done()
  })

  it('errors on missing serverside secret, with undefined', async () => {
    sandbox.stub(config, 'default').value({ SIGNAL_SHARED_SECRET: undefined })
    const response = await register({}, {})
    expect(response.statusCode).to.eq(401)
    expect(response.body).to.include('administrator')
  })

  it('errors on missing serverside secret, with empty string', async () => {
    sandbox.stub(config, 'default').value({ SIGNAL_SHARED_SECRET: '' })
    const response = await register({}, {})
    expect(response.statusCode).to.eq(401)
    expect(response.body).to.include('administrator')
  })

  it('errors when secret doesnt match serverside', async () => {
    sandbox.stub(config, 'default').value({ SIGNAL_SHARED_SECRET: 'nope' })
    const queryStringParameters = { secret: 'hello' }
    const event = { queryStringParameters }
    const context = {}
    const response = await register(event, context)
    expect(response.statusCode).to.eq(401)
    expect(response.body).to.include('not authorized')
  })

  it('errors when invalid json is passed', async () => {
    const queryStringParameters = { secret: 'hello' }
    const event = { queryStringParameters, body: 'totally not json' }
    const context = {}
    const response = await register(event, context)
    expect(response.statusCode).to.eq(400)
    expect(response.body).to.include('failed')
  })

  it('errors when number is missing', async () => {
    sandbox.stub(config, 'default').value({
      SIGNAL_SHARED_SECRET: 'hello',
    })
    const queryStringParameters = { secret: 'hello' }
    const event = { queryStringParameters, body: JSON.stringify({ verificationType: 'SMS' }) }
    const context = {}
    const response = await register(event, context)
    expect(response.statusCode).to.eq(400)
    expect(response.body).to.include('invalid request')
  })

  it('errors when verificationType is missing', async () => {
    sandbox.stub(config, 'default').value({
      SIGNAL_SHARED_SECRET: 'hello',
    })
    const queryStringParameters = { secret: 'hello' }
    const event = { queryStringParameters, body: JSON.stringify({ number: '+111' }) }
    const context = {}
    const response = await register(event, context)
    expect(response.statusCode).to.eq(400)
    expect(response.body).to.include('invalid request')
  })

  it('errors when no verificationType is invalid', async () => {
    sandbox.stub(config, 'default').value({
      SIGNAL_SHARED_SECRET: 'hello',
    })
    const queryStringParameters = { secret: 'hello' }
    const event = { queryStringParameters, body: JSON.stringify({ number: '+111', verificationType: 'telepathy' }) }
    const context = {}
    const response = await register(event, context)
    expect(response.statusCode).to.eq(400)
    expect(response.body).to.include('invalid request')
  })

  it('errors when number is already registered', async () => {
    sandbox.stub(config, 'default').value({
      SIGNAL_SHARED_SECRET: 'hello',
    })
    const queryStringParameters = { secret: 'hello' }
    const event = { queryStringParameters, body: JSON.stringify({ number: '+111', verificationType: 'SMS' }) }
    const context = {}
    // clientMock.expects('registerNumber').once().returns(false)
    clientMock.expects('isNumberRegistered').once().returns(Promise.resolve(true))
    const response = await register(event, context)
    expect(response.statusCode).to.eq(400)
    expect(response.body).to.include('already registered')
    clientMock.verify()
  })

  afterEach((done) => {
    clientMock.restore()
    sandbox.restore()
    done()
  })
})
